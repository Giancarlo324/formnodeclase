const express = require('express'),
      app = express(),
      bodyParser = require('body-parser'); // Extrae toda la parte del cuerpo de una secuencia de solicitud entrante y la expone en req.body.
//const bodyParser = require('body-parser');
const { info } = require('../modules/my-log');
const { countries, languages } = require('countries-list');
//const app = express();

app.use(bodyParser.urlencoded({ extended: false })); // Analiza el texto como datos codificados de URL
app.use(bodyParser.json({ type: 'application/*+json' })); // Analiza el texto como JSON y expone el objeto resultante en req.body.
//Conexión a MongoDB
const mongoose = require('mongoose'); // Para conectar Mongodb.
mongoose.connect('mongodb://localhost/personas', {useNewUrlParser: true, useUnifiedTopology: true});
module.exports = mongoose;

// Verificar conexión de base de datos.
var db = mongoose.connection; 
db.on('error', console.log.bind(console, "db connection error")); 
db.once('open', function(callback){ 
    console.log("db connection succeeded"); 
});

app.listen(4000, () => {
  console.log('Running on http://localhost:4000');
});

app.get('/', (req, res) => {

  var html = '<form action="/registrar" method="POST">'+
      '<div>'+
	    '<label for="name">Ingresa tu nombre: </label>'+
        '<input type="text" id="nombres" name="nombres">'+
      '</div>'+
      '<div>'+
        '<label for="apellidos">Ingresa tus apellidos: </label>'+
		    '<input type="apellidos" id="apellidos" name="apellidos">'+
      '</div>'+
	      '<button type="submit">Enviar</button>'+
    
  '</form>';

  res.status(200).send(html);
});

app.post('/registrar', (req,res) => {
  var nombres = req.body.nombres; 
  var apellidos = req.body.apellidos; 

  var data = { 
      "nombres": nombres, 
      "apellidos":apellidos
  } 
db.collection('usuarios').insertOne(data,function(err, collection){ 
      if (err) throw err; 
      console.log("Datos egistrados correctamente!"); 
            
  }); 
        
  return res.redirect('/exitoso'); 
});

app.get('/exitoso', (req, res) => {
  res.send('<html><body><h1>Registrado correctamente</h1></body></html>');
});

app.get('/info', (req, res) => {
  res.send('<html><body><h1>Info...sdgsdgdf</h1></body></html>');
  info('Hola info');
});

app.get('/country', (req, res) => {
  console.log('req.query: ', req.query);
  //res.send(JSON.stringify(countries[req.query.code]));
  res.json(countries[req.query.code]);
});

app.get('/languages/:lang', (req, res) => {
  console.log('req.params: ', req.params);
  res.json(languages[req.params.lang]);
});

app.get('*', (req, res) => {
  res.status(404).send('<html><body><h1>NO ENCONTRADO</h1></body></html>');
});