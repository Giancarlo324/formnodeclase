const info = (txt) => {
  console.log('INFO: ', txt);
  return txt;
};

const error = (txt) => {
  console.log('ERROR: ', txt);
  return txt;
};

module.exports.info = info;
module.exports.error = error;
